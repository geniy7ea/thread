// ������� 1
// �������� � �����������

//#include <iostream>
//#include <thread>
//#include <chrono>
//#include <ctime>
//#include <mutex>
//#include <vector>
//#include <array>
//#include <random>
//#include <algorithm>
//#include <numeric>
//#include <Windows.h>
//
//using namespace std;
//
//struct ProducerParam
//{
//    int nAmount;
//    int nDelayMs;
//};
//
//constexpr array<ProducerParam, 3> g_arrProducers =
//{ {
//    { 7, 1000 },
//    { 8, 1500 },
//    { 3,  500 },
//} };
//
//struct InfoStruct
//{
//    std::string sName;
//    int         nCount = 0;
//    float       fDuration = 0.f;
//    int         nProducerID = 0;
//};
//
//mutex  g_mtxVector;
//size_t g_nAmount = accumulate(g_arrProducers.cbegin(), g_arrProducers.cend(), 0, [](int s, const ProducerParam& p) { return s + p.nAmount; });
//
//
//// ��������� ��������� ������� �������
//void SetCursorPosition(int x, int y)
//{
//    static const HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
//
//    cout.flush();
//    COORD coord = { (SHORT)x, (SHORT)y };
//    SetConsoleCursorPosition(hOut, coord);
//}
//
//// �������/����������� ������� �������
//void SetCursorVisibility(bool bIsShow)
//{
//    static const HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
//
//    CONSOLE_CURSOR_INFO structCursorInfo;
//    GetConsoleCursorInfo(hOut, &structCursorInfo);
//    structCursorInfo.bVisible = bIsShow;
//    SetConsoleCursorInfo(hOut, &structCursorInfo);
//}
//
//// ��������� ���������� ����� � ��������� [min, max]
//int GetRandomInRange(const int min, const int max)
//{
//    static default_random_engine gen(
//        static_cast<unsigned>(
//            chrono::system_clock::now().time_since_epoch().count()
//            )
//    );
//    uniform_int_distribution<int> distribution(min, max);
//
//    return distribution(gen);
//}
//
//// ��������� ������ �� nMaxLen ��������� ��������
//std::string GetRandomStr(const int nMaxLen)
//{
//    int len = GetRandomInRange(2, nMaxLen);
//
//    std::string tmp_s;
//    static const char alphanum[] =
//        "0123456789"
//        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
//        "abcdefghijklmnopqrstuvwxyz";
//
//    tmp_s.reserve(len);
//
//    for (int i = 0; i < len; ++i)
//        tmp_s += alphanum[GetRandomInRange(0, sizeof(alphanum) - 1)];
//
//    return tmp_s;
//}
//
//// ��������� ������ ����������
//void Producer(vector<InfoStruct>& VectorToFill, int nId)
//{
//    for (int i = 0; i < g_arrProducers[nId].nAmount; ++i)
//    {
//        this_thread::sleep_for(chrono::milliseconds(g_arrProducers[nId].nDelayMs));
//
//        InfoStruct Element;
//        Element.nCount = GetRandomInRange(0, 100);
//        Element.fDuration = GetRandomInRange(0, RAND_MAX) / static_cast<float>(RAND_MAX / 10);
//        Element.nProducerID = nId + 1;
//        Element.sName = GetRandomStr(20);
//
//        lock_guard<mutex> mtx(g_mtxVector);
//        VectorToFill.push_back(Element);
//    }
//}
//
//// ���������� ������ �� ����� (�� �����������)
//void SortVector(vector<InfoStruct>& VectorToSort, bool& bIsAll)
//{
//    lock_guard<mutex> mtx(g_mtxVector);
//
//    sort(VectorToSort.begin(), VectorToSort.end(), [](InfoStruct& a, InfoStruct& b) { return a.sName < b.sName; });
//
//    if (VectorToSort.size() >= g_nAmount)
//        bIsAll = true;
//}
//
//// ����� �� ������� ������
//void PrintVector(const vector<InfoStruct>& VectorToPrint)
//{
//    SetCursorPosition(0, 1);
//
//    lock_guard<mutex> mtx(g_mtxVector);
//
//    for (auto& Element : VectorToPrint)
//        printf("%-20s %5d %10.4f %4d \n", Element.sName.c_str(), Element.nCount, Element.fDuration, Element.nProducerID);
//}
//
//int main()
//{
//    auto StartClock = chrono::high_resolution_clock::now();
//
//    vector<InfoStruct> vInfo;
//    vector<thread>     vThreads;
//
//    // �������� �������
//    for (size_t i = 0; i < g_arrProducers.size(); ++i)
//        vThreads.push_back(thread(Producer, ref(vInfo), i));
//
//    SetCursorVisibility(false);
//
//    // ������ ����� �������
//    printf("%-20s %5s %10s %4s \n", "Name", "Count", "Duration", "ID");
//
//    bool bIsAll = false;
//
//    // ����� �� ������� ��������������� ������
//    while (!bIsAll)
//    {
//        SortVector(vInfo, bIsAll);
//        PrintVector(vInfo);
//    }
//
//    // �������� ���������� ������ �������
//    for (auto& t : vThreads)
//        t.join();
//
//    // ����������� ������ ������� ������
//    chrono::duration<float> DurationClock = chrono::high_resolution_clock::now() - StartClock;
//    cout << "\nDuration: " << DurationClock.count() << endl;
//
//    SetCursorVisibility(true);
//}